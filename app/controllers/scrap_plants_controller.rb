class ScrapPlantsController < ApplicationController
	require 'nokogiri'
  require 'open-uri'
  require 'roo'
  require 'csv'

	def index
		@data = []
		(1..429).each do |page_no|
			url = "http://www.monrovia.com/plant-catalog/search/?start_page="+page_no.to_s+"&common_name=&botanical_name=&sort_by=common_name"
			# url = "http://www.monrovia.com/plant-catalog/search/?start_page=74&common_name=&botanical_name=&sort_by=common_name"
			doc = Nokogiri::HTML(open(url))
			
			doc.css('.list-plant').each do |plant|
				@href = plant.at_css(".image-wrap").search('*[href]').first['href'] rescue nil 
				if @href
					hash = {}
					plant_details = Nokogiri::HTML(open(@href))
					plant_details.css(".plant-main-right").each do |detail|
						plant_name = detail.search('h1').text.gsub(';','') rescue nil 
						plant_desc = detail.search('h2').text.gsub(';','') rescue nil 
						hash[:plant_name] = plant_name
						hash[:plant_detail] = plant_desc
					end

					plant_details.at_css('[id="Overview"]').css(".attribute").each do |plant_overview|
						overview_name = plant_overview.at_css(".label").text.gsub(';','')  rescue nil
						overview_value = plant_overview.at_css(".value").text rescue nil
						hash[overview_name] = overview_value.gsub(';','') 
					end
					plant_details.at_css('[id="Detail"]').css(".attribute").each do |plant_detail|
						detail_name = plant_detail.at_css(".label").text.gsub(';','')  rescue nil 
						detail_value = plant_detail.at_css(".left").text rescue nil 
						hash[detail_name] = detail_value.gsub(';','')  
					end
					plant_details.at_css('[id="Care"]').css(".attribute").each do |plant_care|
						care_name = plant_care.at_css(".label").text.gsub(';','')  rescue nil 
						care_value = plant_care.text.split(care_name).last rescue nil 
						hash[care_name] = care_value.gsub(';','') 
					end
					@data << hash
				end
			end
		end
		respond_to do |format|
			format.html
	    format.csv { send_data to_csv(@data) }
  	end
	end

	def to_csv(data, options = {})
	  CSV.generate(options) do |csv|
	  	headings = data.collect(&:keys).flatten.uniq
	  	csv << headings
	    data.each do |product|
				arranged_info = {}
				headings.each{|head| arranged_info[head] = (product[head] || '') }
	     	csv << arranged_info.values rescue debugger
	    end
	  end
	end
end
