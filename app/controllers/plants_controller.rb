class PlantsController < ApplicationController
	before_action :fetch_plant, only: [:show, :edit, :update, :destroy]
	before_action :prep_html_and_json, only: [:index, :show]
  before_action :get_hardiness_zone, only: [:create, :update]

  def index
		respond_to do |format|
			format.html
			format.json { render json: @plant }
		end
    @plants = Plant.all
    @plant = Plant.new
  end

  def show
  	respond_to do |format|
			format.html
			format.json { render json: @plant }
		end
  end

  def new
    @plant = Plant.new
  end

  def create
    @plant = Plant.new(plant_params) 
    if @plant.save
      create_plant_pictures(params[:plant][:picture]) if params[:plant][:picture].present?
      create_plant_tags(params[:tags]) if params[:tags].present?
      flash[:success] = "Your plant was successfully listed"
      redirect_to plant_path(@plant)
    else
      flash[:error] = "Hmm... something's not quite working with your listing."
      redirect_to plants_path
    end
  end

  def create_plant_pictures(pictures)
    pictures.each do |img|
      @plant.pictures.create(attachment: img)
    end
  end

  def create_plant_tags(tags)
    @plant.tags.destroy_all
    tags.split(",").each do |tag|
      @plant.tags.create(name: tag)
    end
  end

  def edit
  end

  def update
    if @plant.update(plant_params)
      create_plant_pictures(params[:plant][:picture]) if params[:plant][:picture].present?
      create_plant_tags(params[:tags]) if params[:tags].present?
      flash[:success] = "Your plant was successfully updated"
      redirect_to plant_path(@plant)
    else
      flash[:error] = "Hmm... something's not quite working with your listing."
      redirect_to plants_path
    end
  end

  def destroy
    if @plant.destroy
      flash[:success] = "Your plant was successfully deleted"
      redirect_to plants_path
    else
      flash[:error] = "Hmm... something's not quite working with deleting a plant."
      redirect_to plant_path(@plant)
    end
  end

  def destroy_picture
    @picture = Picture.find(params[:plant_id])
    @picture.destroy
    respond_to do |format|
      format.json { render json: @picture }
    end
  end

  def get_hardiness_zone
    params[:plant][:hardiness_zone_from] = params[:hardiness_zone_range].split("_")[0] rescue nil
    params[:plant][:hardiness_zone_to] = params[:hardiness_zone_range].split("_")[1] rescue nil
  end

  private

  def fetch_plant
  	@plant = Plant.find(params[:id])
  end

  def prep_html_and_json
  	respond_to do |format|
			format.html
			format.json { render json: @plant }
		end
  end

  def plant_params
    params.require(:plant).permit(:common_name, :scientific_name, :picture, :hardiness_zone_to, :hardiness_zone_from, :preferred_soil, :light_needs, :water_needs)
  end

end
