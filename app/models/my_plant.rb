class MyPlant < ApplicationRecord
  belongs_to :user
  validates :my_plant_id, presence: true
  validates :common_name, presence: true
end
