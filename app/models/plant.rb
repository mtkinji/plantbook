class Plant < ApplicationRecord
  before_save { common_name.downcase! }
  before_save { scientific_name.downcase! }
  mount_uploader :picture, PictureUploader
  has_many :pictures, dependent: :destroy
  has_many :tags, dependent: :destroy
  validates :common_name, presence: true, uniqueness: { case_sensitive: false }, length: { maximum: 240 }
  validates :scientific_name, presence: true, uniqueness: { case_sensitive: false }, length: { maximum: 240 }
  # validate  :picture_size

  private

  # Validates the size of an uploaded picture.
  def picture_size
    if picture.size > 10.megabytes
      errors.add(:picture, "should be less than 10MB")
    end
  end
end
