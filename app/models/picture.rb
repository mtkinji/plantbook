class Picture < ApplicationRecord
	mount_uploader :attachment, PictureUploader
	belongs_to :plant
	validate  :attachment_size

	def attachment_size
	    if attachment.size > 10.megabytes
	      errors.add(:attachment, "should be less than 10MB")
	    end
  	end
end
