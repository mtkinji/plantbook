module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Plantbook"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def has_admin?(user)
    if user
  	  return user.admin
    else
      return false
    end
  end
end