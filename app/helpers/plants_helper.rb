module PlantsHelper
	def get_tags(plant)
		plant.tags.collect(&:name).join(",")
	end
end
