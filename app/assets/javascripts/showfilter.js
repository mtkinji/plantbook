// This controls the opening and closing of the left side filtering rail on the catalog page.

function openRail() {
   var element = document.getElementById("filterRail");
   element.classList.toggle("open");
}
function changeButtonText() {
 	var text = document.getElementById("filterToggle").innerHTML;

 	if (text === "Show Filters") {
 		document.getElementById("filterToggle").innerHTML = "Hide Filters"
 		console.log(text);
 	} else {
 		document.getElementById("filterToggle").innerHTML = "Show Filters"
 		console.log(text);
 	}
}