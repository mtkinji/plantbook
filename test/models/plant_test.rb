require 'test_helper'

class PlantTest < ActiveSupport::TestCase
 def setup
    @plant = Plant.new(common_name: "Example Plant")
  end

  test "should be valid" do
    assert @plant.valid?
  end

  test "common_name should be present" do
    @plant.common_name = "     "
    assert_not @plant.valid?
  end

  test "name should not be too long" do
    @plant.common_name = "a" * 241
    assert_not @plant.valid?
  end

  test "plant common_name should be unique" do
    duplicate_plant = @plant.dup
    duplicate_plant.common_name = @plant.common_name.upcase
    @plant.save
    assert_not duplicate_plant.valid?
  end

  test "common_name should be saved as lower-case" do
    mixed_case_common_name = "ExaMPlE pLAnt"
    @plant.common_name = mixed_case_common_name
    @plant.save
    assert_equal mixed_case_common_name.downcase, @plant.reload.common_name
  end

end
