class AddLightNeedInPlants < ActiveRecord::Migration[5.1]
  def change
  	add_column :plants, :light_needs, :string
  end
end
