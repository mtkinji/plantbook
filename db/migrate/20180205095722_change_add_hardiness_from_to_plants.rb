class ChangeAddHardinessFromToPlants < ActiveRecord::Migration[5.1]
  def change
  	rename_column :plants, :hardiness_zone, :hardiness_zone_to
  	add_column :plants, :hardiness_zone_from, :integer
  	add_column :plants, :water_needs, :string
  end
end
