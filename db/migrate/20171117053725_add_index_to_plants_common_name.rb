class AddIndexToPlantsCommonName < ActiveRecord::Migration[5.1]
  def change
  	add_index :plants, :common_name, unique: true
  end
end
