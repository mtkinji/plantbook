class CreatePlants < ActiveRecord::Migration[5.1]
  def change
    create_table :plants do |t|
      t.string :common_name
      t.string :scientific_name
      t.integer :hardiness_zone
      t.string :preferred_soil
      t.integer :annual_growth_rate
      t.string :origin
      t.integer :mature_height
      t.integer :mature_width
      t.integer :lifespan_years
      t.boolean :drought_tolerant

      t.timestamps
    end
  end
end
