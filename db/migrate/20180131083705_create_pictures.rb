class CreatePictures < ActiveRecord::Migration[5.1]
  def change
    create_table :pictures do |t|
      t.integer :plant_id
      t.string :attachment

      t.timestamps
    end
  end
end
