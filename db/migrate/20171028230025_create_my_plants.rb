class CreateMyPlants < ActiveRecord::Migration[5.1]
  def change
    create_table :my_plants do |t|
      t.string :common_name
      t.string :scientific_name
      t.string :type
      t.string :address
      t.string :yard_location
      t.string :genus
      t.string :cultivar
      t.string :class
      t.integer :hardiness_zone
      t.text :notes
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
