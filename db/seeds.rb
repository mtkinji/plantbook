# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name:  "Andrew Watanabe",
             email: "mtkinji@gmail.com",
             password:              "password",
             password_confirmation: "password",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

# I hope this works to seed the DB with fake plant data, but I'll test later...
#99.times do |n|
#  name  = Faker::Name.name
#  Plant.create!(common_name:  name,
#               scientific_name: lorem ipsum)
#end