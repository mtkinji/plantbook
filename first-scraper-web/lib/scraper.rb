require 'HTTParty'
require 'Nokogiri'

class Scraper

attr_accessor :parse_page

	def initialize
		doc = HTTParty.get("http://www.monrovia.com/plant-catalog/search/?common_name=A&botanical_name=&sort_by=common_name")
		@parse_page ||= Nokogiri::HTML(doc)
	end

	common_names = parse_page.css("span")
end